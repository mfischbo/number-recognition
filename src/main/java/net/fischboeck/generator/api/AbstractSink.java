package net.fischboeck.generator.api;

import java.security.SecureRandom;

public abstract class AbstractSink implements Sink {

    protected Sink sink;

    protected SecureRandom secureRandom;

    @Override
    public abstract void push(Sample data);

    @Override
    public void setSink(Sink nextSink) {
        this.sink = nextSink;
    }

    void setSecureRandom(SecureRandom random) { this.secureRandom = random; }
}
