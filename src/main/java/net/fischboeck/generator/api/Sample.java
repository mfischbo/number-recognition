package net.fischboeck.generator.api;

import java.util.HashMap;
import java.util.Map;

public class Sample {

    public String string;

    public Map<String, String> labels = new HashMap<>();

    public Sample(String string) {
        this.string = string;
    }
}
