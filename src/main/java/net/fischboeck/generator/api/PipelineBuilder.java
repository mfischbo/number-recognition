package net.fischboeck.generator.api;

import java.security.SecureRandom;
import java.util.LinkedList;

public class PipelineBuilder {

    private LinkedList<Sink> sinks;

    private final SecureRandom secureRandom = new SecureRandom("bananagun".getBytes());

    private PipelineBuilder(Sink sink) {
        if (sink instanceof AbstractSink) {
            ((AbstractSink) sink).setSecureRandom(secureRandom);
        }
        this.sinks = new LinkedList<>();
        this.sinks.add(sink);
    }

    public static PipelineBuilder startWith(Sink sink) {
        PipelineBuilder retval = new PipelineBuilder(sink);
        return retval;
    }

    public PipelineBuilder andAppend(Sink sink) {
        if (sink instanceof AbstractSink) {
            ((AbstractSink) sink).setSecureRandom(this.secureRandom);
        }
        this.sinks.getLast().setSink(sink);
        this.sinks.add(sink);
        return this;
    }

    public Source build() {
        return (Source) this.sinks.getFirst();
    }
}
