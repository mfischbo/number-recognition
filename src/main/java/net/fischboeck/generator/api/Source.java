package net.fischboeck.generator.api;

/**
 * Interface describing a special {@link Sink} to that is able to emit new data.
 */
public interface Source extends Sink {

    /**
     * When called a new {@link Sample} is being created and emitted to the connected {@link Sink}.
     */
    void emit();
}
