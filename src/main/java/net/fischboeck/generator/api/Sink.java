package net.fischboeck.generator.api;

/**
 * A 'piece' of a pipeline.
 * Whenever new data arrives, the sinks {@link Sink#push(Sample)} method will be called.
 * Implementing classes can then transform the incoming data and pass it on to the next {@link Sink} by calling
 * {@link Sink#push(Sample)}.
 *
 */
public interface Sink {

    /**
     * Pass the current piece of data on to the next sink.
     * Note that the sink might be {@code null}.
     *
     * @param data The data to be passed on.
     */
    void push(Sample data);

    /**
     * The {@link Sink} that should follow the current {@link Sink}.
     * @param nextSink The next sink in the pipeline
     */
    void setSink(Sink nextSink);

    /**
     * Sets the sinks secure number generator.
     * @param secureRandom
     */
    //void setSecureRandom(SecureRandom secureRandom);
}
