package net.fischboeck.generator.writer;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class SysOutPrinter extends AbstractSink {

    @Override
    public void push(Sample data) {
        System.out.println(data);

        if (sink != null) {
            sink.push(data);
        }
    }
}
