package net.fischboeck.generator;

public class NCLabels {
    public static final String DECIMAL_SEPARATOR = "decimalSeparator";
    public static final String DECIMAL_COUNT = "decimalCount";
    public static final String GROUPING_SEPARATOR = "groupingSeparator";
}
