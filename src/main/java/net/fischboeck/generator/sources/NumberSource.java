package net.fischboeck.generator.sources;

import lombok.Setter;
import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;
import net.fischboeck.generator.api.Sink;
import net.fischboeck.generator.api.Source;

public class NumberSource extends AbstractSink implements Source {

    @Setter
    private Sink sink;

    public void push(Sample data) {}

    public void emit() {

        int number = 0;
        int rnd = secureRandom.nextInt(10);
        if (rnd <= 4) {
            number = secureRandom.nextInt(99);
        } else if (rnd > 4 && rnd < 8) {
            number = secureRandom.nextInt(10_000);
        } else {
            number = secureRandom.nextInt(5_000_000);
        }

        int negative = this.secureRandom.nextInt(5);

        if (negative == 3) {
            number = number * -1;
        }
        if (this.sink != null) {
            String val = Integer.toString(number);
            Sample data = new Sample(val);
            this.sink.push(data);
        }
    }
}
