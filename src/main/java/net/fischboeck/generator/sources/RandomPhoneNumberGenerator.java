package net.fischboeck.generator.sources;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class RandomPhoneNumberGenerator extends AbstractSink {

    int ratio;

    public RandomPhoneNumberGenerator(int ratio) {
        this.ratio = ratio;
    }

    @Override
    public void push(Sample data) {

        if (secureRandom.nextInt(ratio) == ratio / 2) {
            int num1, num2, num3; //3 numbers in area code
            int set2, set3; //sequence 2 and 3 of the phone number

            //Area code number; Will not print 8 or 9
            num1 = secureRandom.nextInt(7) + 1; //add 1 so there is no 0 to begin
            num2 = secureRandom.nextInt(8); //randomize to 8 becuase 0 counts as a number in the generator
            num3 = secureRandom.nextInt(8);

            // Sequence two of phone number
            // the plus 100 is so there will always be a 3 digit number
            // randomize to 643 because 0 starts the first placement so if i randomized up to 642 it would only go up yo 641 plus 100
            // and i used 643 so when it adds 100 it will not succeed 742
            set2 = secureRandom.nextInt(643) + 100;

            //Sequence 3 of numebr
            // add 1000 so there will always be 4 numbers
            //8999 so it wont succed 9999 when the 1000 is added
            set3 = secureRandom.nextInt(8999) + 1000;

            String val = "(" + num1 + "" + num2 + "" + num3 + ")" + "-" + set2 + "-" + set3;

            if (sink != null) {
                sink.push(new Sample(val));
            }
        } else {
            if (sink != null) {
                sink.push(data);
            }
        }
    }
}
