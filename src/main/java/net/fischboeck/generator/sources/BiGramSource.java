package net.fischboeck.generator.sources;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.InputStream;

public class BiGramSource extends AbstractSink {

    private String[] dictionary;
    private int inputRatio;

    public BiGramSource(InputStream textFile, int inputRatio) {
        try {
            IOUtils.readLines(textFile, "UTF-8").forEach(line -> {
                String[] words = line.split("\\s");
                dictionary = ArrayUtils.addAll(dictionary, words);
            });

        } catch (Exception ex) {
            System.err.println("Unable to read input file");
        }

        this.inputRatio = inputRatio;
    }

    @Override
    public void push(Sample data) {

        if (sink != null) {
            for (int i = 0; i < inputRatio; i++) {
                int rn1 = secureRandom.nextInt(dictionary.length);
                int rn2 = secureRandom.nextInt(dictionary.length);

                Sample sample = new Sample(dictionary[rn1] + " " + dictionary[rn2]);
                sink.push(sample);
            }

            sink.push(data);
        }
    }
}
