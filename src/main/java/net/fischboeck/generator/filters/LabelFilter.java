package net.fischboeck.generator.filters;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class LabelFilter extends AbstractSink {

    private String label;
    private String value;

    public LabelFilter(String label, String value) {
        this.label = label;
        this.value = value;
    }

    @Override
    public void push(Sample data) {

        if (!data.labels.containsKey(label)) {
            return;
        }

        if (!data.string.contains(value)) {
            return;
        }

        if (data.labels.get(label).equals(value) && sink != null) {
            sink.push(data);
        }
    }
}
