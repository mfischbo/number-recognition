package net.fischboeck.generator.filters;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class NumberRangeFilter extends AbstractSink {

    private final double min;
    private final double max;

    public NumberRangeFilter(double min, double max) {
        this.min = min;
        this.max = max;
    }


    @Override
    public void push(Sample data) {

        try {
            Double d = Double.parseDouble(data.string);
            if ((d >= min && d <= max) && sink != null) {
                sink.push(data);
            }
        } catch (Exception ex) {
            if (sink != null) {
                sink.push(data);
            }
        }
    }
}
