package net.fischboeck.generator.filters;


import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class StringLengthFilter extends AbstractSink {

    private int maxChars;

    public StringLengthFilter(int maxChars) {
        this.maxChars = maxChars;
    }

    @Override
    public void push(Sample data) {

        if (sink != null && data.string.length() <= maxChars) {
            sink.push(data);
        }
    }
}
