package net.fischboeck.generator.editors;


import net.fischboeck.generator.NCLabels;
import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class DecimalAdder extends AbstractSink {

    @Override
    public void push(Sample data) {

        String val = data.string;
        int rnd = this.secureRandom.nextInt(100);

        // by a chance of 80% add two decimal digits to the end
        if (rnd < 80) {

            int num = secureRandom.nextInt(99);

            if (num < 10) {
                val += ".0" + num;
            } else {
                val += "." + num;
            }

            data.labels.put(NCLabels.DECIMAL_SEPARATOR, ".");
            data.labels.put(NCLabels.DECIMAL_COUNT, "2");
        }

        if (sink != null) {
            data.string = val;
            sink.push(data);
        }
    }
}
