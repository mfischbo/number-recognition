package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InsertBetweenDigits extends AbstractSink {

    private static final Pattern pattern = Pattern.compile("(\\d)(\\d)(.*)");

    private int randomRange;
    private String insert;

    public InsertBetweenDigits(String string, int randomRange) {
        this.randomRange = randomRange;
        this.insert = string;
    }

    @Override
    public void push(Sample data) {

        int i = secureRandom.nextInt(randomRange);
        if (i == randomRange / 2) {

            Matcher m = pattern.matcher(data.string);
            if (m.find()) {
                String newval = m.group(1) + insert + m.group(2) + m.group(3);
                data.string = newval;
            }
        }

        if (sink != null) {
            sink.push(data);
        }
    }
}
