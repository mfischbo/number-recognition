package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.ArrayUtils;

import java.io.InputStream;
import java.security.SecureRandom;

public class AppendText extends AbstractSink {

    private String[] dictionary = new String[0];
    private int maxWords;
    private int randomRange;

    public AppendText(InputStream textFile, int maxWords, int randomRange) {

        try {
            IOUtils.readLines(textFile, "UTF-8").forEach(line -> {
                String[] words = line.split("\\s");
                dictionary = ArrayUtils.addAll(dictionary, words);
            });

        } catch (Exception ex) {
            System.err.println("Unable to read input file");
        }

        this.maxWords = maxWords;
        this.secureRandom = new SecureRandom();
        this.randomRange = randomRange;
    }

    @Override
    public void push(Sample data) {

        int i = secureRandom.nextInt(randomRange);
        if (i != randomRange / 2) {

            int count = secureRandom.nextInt(maxWords);
            String suffix = "";
            for (i=0; i < count; i++) {
                suffix += " " + dictionary[secureRandom.nextInt(dictionary.length -1)];
            }

            data.string = data.string + suffix;
        }

        if (sink != null) {
            sink.push(data);
        }
    }
}
