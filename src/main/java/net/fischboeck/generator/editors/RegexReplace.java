package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class RegexReplace extends AbstractSink {

    private int randomRange;

    private String regex;
    private String replace;

    public RegexReplace(String regex, String replace, int randomRange) {
        this.regex = regex;
        this.replace = replace;
        this.randomRange = randomRange;
    }

    @Override
    public void push(Sample data) {

        int rnd = secureRandom.nextInt(randomRange);
        if (rnd == randomRange / 2) {

            String value = data.string.replaceAll(regex, replace);
            data.string = value;
        }

        if (sink != null) {
            sink.push(data);
        }
    }
}
