package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class RandomizedPercentAdder extends AbstractSink {


    @Override
    public void push(Sample data) {

        // chance of 3:5 that it's going to be added
        int addRnd = secureRandom.nextInt(5);
        if (addRnd < 3) {

            String space = randomSpace();
            String symbolOrWord = randomSymbolOrWord();
            data.string += space + symbolOrWord;
            data.labels.put("percent", "yes");
        }

        if (sink != null) {
            sink.push(data);
        }
    }


    private String randomSymbolOrWord() {

        boolean rndSym = secureRandom.nextBoolean();
        if (rndSym) {
            return "%";
        }

        // then add the word 'percent' ... in different languages
        boolean rndLang = secureRandom.nextBoolean();
        if (rndLang) {
            return "Prozent";
        }
        return "percent";
    }


    private String randomSpace() {

        String space = "";
        int spaceRnd = secureRandom.nextInt(100);
        if (spaceRnd < 50) {
            space = "\u0020";
        }
        if (spaceRnd >= 50) {
            space = "\u00A0";
        }
        if (spaceRnd == 42) {
            space = "";
        }
        return space;
    }
}
