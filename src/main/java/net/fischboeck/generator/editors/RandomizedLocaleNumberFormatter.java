package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;
import net.fischboeck.generator.NCLabels;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

public class RandomizedLocaleNumberFormatter extends AbstractSink {

    static final List<DecimalFormatSymbols> symbols = new ArrayList<>();
    static final List<String> patterns = new ArrayList<>();

    static {
        symbols.add(new DecimalFormatSymbols(Locale.UK));
        symbols.add(new DecimalFormatSymbols(Locale.GERMANY));
        symbols.add(new DecimalFormatSymbols(Locale.ITALY));

        DecimalFormatSymbols symFR = new DecimalFormatSymbols(Locale.FRANCE);
        symFR.setGroupingSeparator('\u0020');
        symbols.add(symFR);


        DecimalFormatSymbols symCH = new DecimalFormatSymbols(Locale.GERMANY);
        symCH.setGroupingSeparator('\'');
        symCH.setCurrency(Currency.getInstance("CHF"));
        symbols.add(symCH);


        DecimalFormatSymbols symCHAlternate = new DecimalFormatSymbols(Locale.GERMANY);
        symCHAlternate.setGroupingSeparator('\u00A0');
        symCHAlternate.setCurrency(Currency.getInstance("CHF"));
        symbols.add(symCHAlternate);

        patterns.add("###,###.00");
    }


    @Override
    public void push(Sample data) {

        DecimalFormatSymbols sym = symbols.get(secureRandom.nextInt(symbols.size()));
        DecimalFormat fmt = new DecimalFormat(patterns.get(secureRandom.nextInt(patterns.size())), sym);
        Double amount = Double.parseDouble(data.string);

        data = format(sym, fmt, amount, data);

        if (sink != null) {
            sink.push(data);
        }
    }

    private Sample format(DecimalFormatSymbols symbol, DecimalFormat fmt, double amount, Sample data) {

        data.string = fmt.format(amount);
        data.labels.put(NCLabels.DECIMAL_SEPARATOR, symbol.getDecimalSeparator() + "");
        data.labels.put(NCLabels.GROUPING_SEPARATOR, symbol.getGroupingSeparator() + "");

        if (amount < 1000.d && amount > -1000.d) {
            data.labels.remove(NCLabels.GROUPING_SEPARATOR);
        }

        //data.labels.put("C", symbol.getCurrency().getCurrencyCode());
        return data;
    }
}
