package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class FindAndReplace extends AbstractSink {

    private String find;
    private String replace;
    private int randomRange;


    public FindAndReplace(String find, String replace, int randomRange) {
        this.find = find;
        this.replace = replace;
        this.randomRange = randomRange;
    }

    @Override
    public void push(Sample data) {

        if (data.string.contains(find)) {
            int rnd = secureRandom.nextInt(randomRange);
            if (rnd == randomRange / 2) {
                String value = data.string.replace(find, replace);
                data.string = value;
            }
        }

        if (sink != null) {
            sink.push(data);
        }
    }
}
