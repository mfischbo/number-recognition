package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class ToLowerCase extends AbstractSink {

    private int randomRange = 0;

    public ToLowerCase(int randomRange) {
        this.randomRange = randomRange;
    }

    @Override
    public void push(Sample data) {

        if (secureRandom.nextInt(randomRange) == randomRange / 2) {
            data.string = data.string.toLowerCase();
        }

        if (sink != null) {
            sink.push(data);
        }
    }
}
