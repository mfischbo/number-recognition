package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

public class RandomizedCurrencyAdder extends AbstractSink {

    private List<String> currencyCodes;
    private List<String> currencySymbols;


    public RandomizedCurrencyAdder() {

        this.currencyCodes = new ArrayList<>();
        this.currencyCodes.addAll(
                Currency.getAvailableCurrencies().stream()
                    .map(cu -> cu.getCurrencyCode())
                        .collect(Collectors.toList()));

        this.currencySymbols = new ArrayList<>();
        this.currencySymbols.addAll(
                Currency.getAvailableCurrencies().stream()
                .map(cu -> cu.getSymbol())
                .collect(Collectors.toList())
        );
    }


    @Override
    public void push(Sample data) {

        int rnd = secureRandom.nextInt(currencyCodes.size());
        String code;

        if (secureRandom.nextBoolean()) {
            code = currencyCodes.get(rnd);
        } else {
            code = currencySymbols.get(rnd);
        }

        if (secureRandom.nextBoolean()) {
            data.string = code + "\u0020" + data.string;
        } else {
            data.string = data.string + "\u0020" + code;
        }

        if (sink != null) {
            sink.push(data);
        }
    }
}
