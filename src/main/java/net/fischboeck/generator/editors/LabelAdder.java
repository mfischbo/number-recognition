package net.fischboeck.generator.editors;

import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

public class LabelAdder extends AbstractSink {

    private final String label;
    private final String value;
    private final boolean override;

    public LabelAdder(String label, String value, boolean override) {
        this.label = label;
        this.value = value;
        this.override = override;
    }

    @Override
    public void push(Sample data) {

        if (override) {
            data.labels.put(label, value);
        } else {
            if (!data.labels.containsKey(label)) {
                data.labels.put(label, value);
            }
        }
        if (sink != null) {
            sink.push(data);
        }
    }
}
