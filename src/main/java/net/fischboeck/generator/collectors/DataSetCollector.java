package net.fischboeck.generator.collectors;

import net.fischboeck.c45.trainer.DataSet;
import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

import java.util.LinkedList;
import java.util.List;

public class DataSetCollector extends AbstractSink {

    private List<DataSet.Entry> dataSetEntries = new LinkedList<>();
    private String labelToCollect;

    public DataSetCollector(String label) {
        this.labelToCollect = label;
    }

    @Override
    public void push(Sample data) {


        String label = data.labels.get(labelToCollect);
        this.dataSetEntries.add(new DataSet.Entry(data.string, label));

        if (sink != null) {
            sink.push(data);
        }
    }

    public List<DataSet.Entry> getCollectedEntries() {
        return this.dataSetEntries;
    }
}
