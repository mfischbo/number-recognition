package net.fischboeck.generator.collectors;

import lombok.Data;
import net.fischboeck.generator.api.AbstractSink;
import net.fischboeck.generator.api.Sample;

import java.util.LinkedList;
import java.util.List;

@Data
public class SampleBucket extends AbstractSink {

    private List<Sample> examples = new LinkedList<>();

    @Override
    public void push(Sample data) {
        this.examples.add(data);
    }
}
