package net.fischboeck.classifiers;

import net.fischboeck.c45.features.*;
import net.fischboeck.c45.trainer.Classifier;
import net.fischboeck.c45.trainer.DataSet;
import net.fischboeck.c45.trainer.ValidationResult;
import net.fischboeck.generator.api.PipelineBuilder;
import net.fischboeck.generator.api.Source;
import net.fischboeck.generator.collectors.DataSetCollector;
import net.fischboeck.generator.editors.*;
import net.fischboeck.generator.sources.BiGramSource;
import net.fischboeck.generator.sources.NumberSource;
import net.fischboeck.generator.sources.RandomPhoneNumberGenerator;
import org.apache.commons.text.RandomStringGenerator;

import java.util.Random;

public class CurrencyClassifier {

    public static void main(String[] args) {

        Classifier isCurrencyClassifier = createClassifier(1500);

        ValidationResult result = isCurrencyClassifier.validate();
        result.print();

        System.out.println("\n\n\n--------- Classifications ------------ ");
        System.out.println(isCurrencyClassifier.classify("Call: +(49) 114 146 146"));
        System.out.println(isCurrencyClassifier.classify(""));
        System.out.println(isCurrencyClassifier.classify("28.09.2017"));
        System.out.println(isCurrencyClassifier.classify("39,00 km"));
        System.out.println(isCurrencyClassifier.classify("82,41 Prozent"));
        System.out.println(isCurrencyClassifier.classify("82.41 %"));
    }


    public static Classifier createClassifier(int testDataSize) {

        Random rnd = new Random();
        RandomStringGenerator randomNumericGen = new RandomStringGenerator.Builder()
                .withinRange('0', '9')
                .usingRandom(rnd::nextInt)
                .build();

        DataSetCollector collector = new DataSetCollector("currency");

        Source pipeline = PipelineBuilder.startWith(new NumberSource())
                .andAppend(new DecimalAdder())
                .andAppend(new RandomizedLocaleNumberFormatter())
                .andAppend(new RandomizedCurrencyAdder())

                // introduce errors
                .andAppend(new FindAndReplace(".", "'", 5))
                .andAppend(new FindAndReplace(".", " ", 5))
                .andAppend(new FindAndReplace(",", ".", 5))
                .andAppend(new FindAndReplace("-", "\u2013", 20))

                // distorts grouping widths
                .andAppend(new InsertBetweenDigits(randomNumericGen.generate(1), 1000))

                // interchange ,00 with ,\u2013
                .andAppend(new RegexReplace("[,\\.]\\d\\d[\u0020\u00A0]", ",\u2013\u0020", 500))

                // accidently remove whitespace between currency and digits
                .andAppend(new RegexReplace("EUR[\u0020\u00A0]", "EUR", 5000))
                .andAppend(new RegexReplace("\\d[\u0020\u00A0]EUR", "EUR", 5000))

                // mark those as currency codes
                .andAppend(new LabelAdder("currency", "yes", false))

                // add some random text and mark those samples as non currencies
                .andAppend(new BiGramSource(CurrencyClassifier.class.getResourceAsStream("/monkeytyper/text-with-digits.txt"), 9))
                .andAppend(new RandomPhoneNumberGenerator(20))
                .andAppend(new LabelAdder("currency", "no", false))
                .andAppend(collector)
                .build();

        for (int i=0; i < testDataSize; i++) {
            pipeline.emit();
        }


        Feature[] features = new Feature[] {
                new ContainsDigitFeature(),
                new ContainsCurrencyCode(),
                new MaxDigitGroupLength(),
                new DigitRatioFeature(),
                new ContainsSpecialChars(),
                new DigitCountFeature()
        };
        DataSet ds = new DataSet(collector.getCollectedEntries(), 30.d);

        Classifier isCurrencyClassifier =  new Classifier.Builder()
                .forDataset(ds)
                .withPruningEnabled()
                .withFeatures(features)
                .withPositiveMatchesLabeledAs("yes")
                .withNegativeMatchesLabeledAs("no")
                .build();

        isCurrencyClassifier.train();
        return isCurrencyClassifier;
    }
}
