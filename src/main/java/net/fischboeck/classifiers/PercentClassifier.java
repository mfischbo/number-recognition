package net.fischboeck.classifiers;

import net.fischboeck.c45.features.ContainsPercentSymbolOrWordFeature;
import net.fischboeck.c45.features.DigitCountFeature;
import net.fischboeck.c45.features.Feature;
import net.fischboeck.c45.trainer.Classifier;
import net.fischboeck.c45.trainer.DataSet;
import net.fischboeck.c45.trainer.ValidationResult;
import net.fischboeck.generator.api.PipelineBuilder;
import net.fischboeck.generator.api.Source;
import net.fischboeck.generator.collectors.DataSetCollector;
import net.fischboeck.generator.editors.DecimalAdder;
import net.fischboeck.generator.editors.LabelAdder;
import net.fischboeck.generator.editors.RandomizedPercentAdder;
import net.fischboeck.generator.editors.RegexReplace;
import net.fischboeck.generator.filters.NumberRangeFilter;
import net.fischboeck.generator.sources.BiGramSource;
import net.fischboeck.generator.sources.NumberSource;

public class PercentClassifier {

    public static void main(String[] args) {
        PercentClassifier.createClassifier();
    }

    public static Classifier createClassifier() {

        int samples = 1500;

        DataSetCollector collector = new DataSetCollector("percent");

        Source source = PipelineBuilder.startWith(new NumberSource())
                // create actual percentage values
                .andAppend(new DecimalAdder())
                .andAppend(new NumberRangeFilter(-100.d, 100.d))
                .andAppend(new RandomizedPercentAdder())

                // introduce some errors
                .andAppend(new RegexReplace("Prozent", "Porzent", 10))
                .andAppend(new RegexReplace("\\.", ",", 4))
                .andAppend(new RegexReplace("-", "\u2013", 8))

                // append some text
                .andAppend(new BiGramSource(PercentClassifier.class.getResourceAsStream("/monkeytyper/text-with-digits.txt"), 9))
                .andAppend(new LabelAdder("percent", "no", false))
                .andAppend(collector)

                .build();

        for (int i=0; i < samples; i++) {
            source.emit();
        }

        Feature[] features = new Feature[] {
                new DigitCountFeature(),
                new ContainsPercentSymbolOrWordFeature(),
        };

        Classifier percentClassifier = new Classifier.Builder()
                .forDataset(new DataSet(collector.getCollectedEntries(), 25.d))
                .withNegativeMatchesLabeledAs("no")
                .withPositiveMatchesLabeledAs("yes")
                .withPruningEnabled()
                .withFeatures(features)
                .build();

        percentClassifier.train();
        ValidationResult result = percentClassifier.validate();
        result.print();

        return percentClassifier;
    }
}
