package net.fischboeck.c45.util;

public class Constants {

    public static final char[] Whitespace = new char[] {
            '\u0009',
            '\u000B', //  line tabulation
            '\u000C', // form feed
            '\u0020', //space
            '\u0085', //next line
            '\u00A0', //no-break space
            '\u1680', //ogham space mark
            '\u180E', //  mongolian vowel separator
            '\u2000', //en quad
            '\u2001', //em quad
            '\u2002', //en space
            '\u2003', //em space
            '\u2004', //three-per-em space
            '\u2005', //four-per-em space
            '\u2006', //six-per-em space
            '\u2007', //figure space
            '\u2008', //punctuation space
            '\u2009', //thin space
            '\u200A', // hair space
            '\u200B', // zero width space
            '\u200C', //  zero width non-joiner
            '\u200D', //  zero width joiner
            '\u2028', //line separator
            '\u2029', //paragraph separator
            '\u202F', //  narrow no-break space
            '\u205F', //  medium mathematical space
            '\u2060', //word joiner
            '\u3000', //ideographic space
            '\uFEFF'  //  zero width non-breaking space
    };

    public static final String WhitespaceString;

    static {
        StringBuilder b = new StringBuilder();
        for (char c : Whitespace) {
            b.append(c);
        }
        WhitespaceString = b.toString();
    }
}
