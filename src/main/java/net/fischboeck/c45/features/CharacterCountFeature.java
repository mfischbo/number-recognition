package net.fischboeck.c45.features;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * This feature counts the occurrences of the specified character in the input string.
 */
public class CharacterCountFeature implements Feature {

    private char search;

    public CharacterCountFeature(char search) {
        this.search = search;
    }

    @Override
    public String getName() {
        return "count["+search+"]";
    }

    @Override
    public double getValue(String input) {

        int retval = 0;

        CharacterIterator iterator = new StringCharacterIterator(input);
        while (iterator.current() != CharacterIterator.DONE) {

            if (iterator.current() == search) {
                retval++;
            }
            iterator.next();
        }
        return retval;
    }
}
