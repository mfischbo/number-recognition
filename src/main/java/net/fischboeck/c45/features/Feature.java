package net.fischboeck.c45.features;

import java.io.Serializable;

/**
 * Interface describing a feature for a C45 classifier.
 * Each feature must return a unique name amongst all features (or at least within a classifier setup).
 * Each features returns a double value used for classifying the data.
 */
public interface Feature extends Serializable {

    /**
     * The name of the feature.
     * A unique (non-null, not empty) string.
     * @return The name of the feature.
     */
    String getName();

    /**
     * During training the method will be called with the test data instance (the example).
     * Implementing classes need to return a double value that represent s the value of the feature given the input.
     * Example: A feature counting the length of the input string would return 5.0d for input: 'Hello'
     * Features that represent a 'true' or 'false' evaluation can express this by returning either 1 or 0.
     *
     * @param input The test data instance.
     * @return The value this feature represents.
     */
    double getValue(String input);
}
