package net.fischboeck.c45.features;

/**
 * The feature detects the presence / absence of
 * a) the percentage symbol
 * b) The word for 'percent' in either english or german
 *
 * If the feature detects the presence of either both it returns 1.d, otherwise 0.d
 */
public class ContainsPercentSymbolOrWordFeature implements Feature {

    @Override
    public String getName() {
        return "percentSymWord";
    }

    @Override
    public double getValue(String input) {

        if (input.contains("%")) {
            return 1.d;
        }

        if (input.toLowerCase().contains("prozent")) {
            return 1.d;
        }

        if (input.toLowerCase().contains("percent")) {
            return 1.d;
        }
        return 0.d;
    }
}
