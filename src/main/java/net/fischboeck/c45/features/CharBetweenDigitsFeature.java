package net.fischboeck.c45.features;

import java.util.regex.Pattern;

/**
 * This feature detects whether or not a specified character can be found between (groups) of digits.
 * If so the feature returns 1.d otherwise it return 0.d
 */
public class CharBetweenDigitsFeature implements Feature {

    private char character;
    private Pattern p;

    public CharBetweenDigitsFeature(char character) {
        this.character = character;
        String token = "" + character;

        if (character == '.')
            token = "\\.";

        p = Pattern.compile("\\d+" + character + "\\d+");
    }

    @Override
    public String getName() {
        return "CharBetweenDigits{" + this.character + "}";
    }

    @Override
    public double getValue(String input) {

        if (p.matcher(input).find()) {
            return 1;
        }
        return 0;
    }
}
