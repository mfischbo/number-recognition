package net.fischboeck.c45.features;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This feature detects the presence/absence of a currency code or symbol from {@link Currency#getAvailableCurrencies()}.
 * If a currency code or symbol is present the feature returns 1.d otherwise it returns 0.d
 */
public class ContainsCurrencyCode implements Feature {

    private List<String> currencyCodes = new ArrayList<>();

    public ContainsCurrencyCode() {

        currencyCodes.addAll(
                Currency.getAvailableCurrencies().stream()
                .map(cu -> cu.getCurrencyCode().toLowerCase())
                .collect(Collectors.toList()));

        currencyCodes.addAll(
                Currency.getAvailableCurrencies().stream()
                        .map(cu -> cu.getSymbol().toLowerCase())
                        .collect(Collectors.toList()));

    }

    @Override
    public String getName() {
        return "cc-code";
    }

    @Override
    public double getValue(String input) {

        for (String code : currencyCodes) {
            if (input.toLowerCase().contains(code.toLowerCase())) {
                return 1.0d;
            }
        }
        return 0.0d;
    }
}
