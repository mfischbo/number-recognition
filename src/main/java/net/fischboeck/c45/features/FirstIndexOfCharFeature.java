package net.fischboeck.c45.features;

import org.apache.commons.lang3.StringUtils;

/**
 * Returns the position of the first occurrence of the specified character.
 */
public class FirstIndexOfCharFeature implements Feature {

    private char search;
    private boolean reverse;

    public FirstIndexOfCharFeature(char search, boolean reverse) {
        this.search = search;
        this.reverse = reverse;
    }

    @Override
    public String getName() {
        return "firstidx{"+this.search+"}";
    }

    @Override
    public double getValue(String input) {

        if (reverse) {
            input = StringUtils.reverse(input);
        }

        return input.indexOf(search);
    }
}
