package net.fischboeck.c45.features;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * Counts and returns the amount of digit characters as specified by ({@link Character#isDigit(char)}) in the input.
 */
public class DigitCountFeature implements Feature {

    @Override
    public String getName() {
        return "digitcount";
    }

    @Override
    public double getValue(String input) {

        CharacterIterator it = new StringCharacterIterator(input);
        int count = 0;
        while (it.current() != CharacterIterator.DONE) {

            if (Character.isDigit(it.current())) {
                count++;
            }

            it.next();
        }
        return count;
    }
}
