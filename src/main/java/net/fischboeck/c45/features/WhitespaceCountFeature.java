package net.fischboeck.c45.features;

import net.fischboeck.c45.util.Constants;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * Counts and returns the amount of whitespace characters.
 * As whitespace character considered are all characters that are defined as whitespace by in the UTF-8 space.
 * Note that this does not use {@link Character#isWhitespace(char)}, since the implementation will not recognize all
 * whitespace characters in the UTF-8 space.
 */
public class WhitespaceCountFeature implements Feature {

    @Override
    public String getName() {
        return "whitespaces";
    }

    @Override
    public double getValue(String input) {

        int retval = 0;

        CharacterIterator it = new StringCharacterIterator(input);
        while (it.current() != CharacterIterator.DONE) {

            char current = it.current();
            for (int i = 0; i < Constants.Whitespace.length; i++) {
                if (current == Constants.Whitespace[i]) {
                    retval++;
                }
            }
            it.next();
        }
        return retval;
    }
}
