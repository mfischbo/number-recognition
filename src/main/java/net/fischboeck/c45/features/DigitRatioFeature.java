package net.fischboeck.c45.features;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * This feature calculates the ratio between digit to non-digit characters.
 */
public class DigitRatioFeature implements Feature {

    @Override
    public String getName() {
        return "digitRatio";
    }

    @Override
    public double getValue(String input) {

        if (input.length() == 0) {
            return 0;
        }

        int digits = 0;
        CharacterIterator it = new StringCharacterIterator(input);
        while (it.current() != CharacterIterator.DONE) {

            if (Character.isDigit(it.current())) {
                digits++;
            }
            it.next();
        }
        return (digits*100) / input.length();
    }
}
