package net.fischboeck.c45.features;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * This feature detects the presence / absence of a digit in the input.
 * If a digit is present the feature returns 1.d otherwise 0.d
 */
public class ContainsDigitFeature implements Feature {

    @Override
    public String getName() {
        return "containsDigit";
    }

    @Override
    public double getValue(String input) {

        CharacterIterator it = new StringCharacterIterator(input);
        while (it.current() != CharacterIterator.DONE) {

            if (Character.isDigit(it.current())) {
                return 1.0;
            }
            it.next();
        }
        return 0.0d;
    }
}
