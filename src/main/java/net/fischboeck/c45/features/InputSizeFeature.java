package net.fischboeck.c45.features;

/**
 * This feature returns the length of the given input.
 */
public class InputSizeFeature implements Feature {

    @Override
    public String getName() {
        return "inputSize";
    }

    @Override
    public double getValue(String input) {

        if (input == null) {
            return 0.d;
        }
        return input.length();
    }
}
