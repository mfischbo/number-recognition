package net.fischboeck.c45.features;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

/**
 * Detects the presence / absence of special characters (as specified by the inverse operation of
 * {@link Character#isLetterOrDigit(char)}.
 * If present, the feature return 1.d otherwise 0.d
 */
public class ContainsSpecialChars implements Feature {


    @Override
    public String getName() {
        return "contains-special";
    }

    @Override
    public double getValue(String input) {

        CharacterIterator it = new StringCharacterIterator(input);
        while (it.current() != CharacterIterator.DONE) {

            if (!Character.isLetterOrDigit(it.current())) {
                return 1.0d;
            }
            it.next();
        }
        return 0.0d;
    }
}
