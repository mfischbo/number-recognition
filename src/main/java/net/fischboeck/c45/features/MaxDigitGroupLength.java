package net.fischboeck.c45.features;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This feature returns the length of the longest "group" of digits found.
 * A "group" of digits is considered to be any sequence of digits that are not separated by a non digit character.
 */
public class MaxDigitGroupLength implements Feature {

    private static final Pattern p = Pattern.compile("([\\d]+)");

    @Override
    public String getName() {
        return "MaxDigitGroupLength";
    }

    @Override
    public double getValue(String input) {

        int groupSize = 0;
        Matcher m = p.matcher(input);

        while (m.find()) {
            for (int i=0; i < m.groupCount(); i++) {
                groupSize = Math.max(groupSize, m.group(i).length());
            }
        }
        return groupSize;
    }
}
