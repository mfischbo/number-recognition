package net.fischboeck.c45.trainer;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.*;

@Getter
public class DataSet {

    private List<Entry> trainingSet = new ArrayList<>();
    private List<Entry> validationSet = new ArrayList<>();


    /**
     * Creates a new dataset
     * @param entries The entries to be added to the dataset
     * @param evaluationRatio The ratio (in percent) on how many entries should be used for evaluation.
     */
    public DataSet(Collection<Entry> entries, double evaluationRatio) {
        List<Entry> fullSet = new LinkedList<>();
        fullSet.addAll(entries);
        Collections.shuffle(fullSet);

        int evalSize = (int) Math.floor((evaluationRatio * fullSet.size()) / 100.d);
        if (evalSize == 0) {
            this.trainingSet.addAll(fullSet);
        } else {
            this.validationSet.addAll(fullSet.subList(0, evalSize));
            this.trainingSet.addAll(fullSet.subList(evalSize - 1, fullSet.size() - 1));
        }
    }

    @Getter
    @AllArgsConstructor
    public static class Entry {
        String token;
        String label;
    }
}
