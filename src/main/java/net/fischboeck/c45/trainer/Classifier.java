package net.fischboeck.c45.trainer;

import cc.mallet.classify.C45;
import cc.mallet.classify.C45Trainer;
import cc.mallet.classify.Classification;
import cc.mallet.pipe.Pipe;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import lombok.Getter;
import net.fischboeck.c45.features.Feature;
import net.fischboeck.c45.io.PipeFactory;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * A classifier using a C4.5 algorithm underneath.
 */
public class Classifier implements Serializable {

    /**
     * Whether or not pruning is enabled
     */
    private boolean doPruning = false;

    /**
     * The list of features to label the data with
     */
    private List<Feature> features;

    /**
     * The data set after it has been read.
     */
    private transient DataSet data;

    /**
     * The pipeline used to transform the data. Also holds the 'feature alphabet'.
     * Note: It's important that a classifier (this.tree) always uses the same pipe, since it has internal state
     * that is required for classification after training.
     */
    private Pipe pipe;

    /**
     * The C45 tree after the classifier has been trained.
     */
    private C45 tree;

    @Getter
    private String positiveMatchLabel = "yes";

    @Getter
    private String negativeMatchLabel = "no";

    /**
     * Internal flag to indicate that the classifier has been deserialized from an external source.
     * In this case the transient data set is not available anymore and thus any methods accessing this data
     * can't be executed.
     */
    @Getter
    private boolean locked = false;

    /**
     * States if the classifier has been trainied.
     */
    @Getter
    private boolean trained = false;

    /**
     * Constructs new classifier with the specified dataset.
     * @param dataSet The dataset to operate on.
     */
    private Classifier(DataSet dataSet) {
        this.data = dataSet;
        this.pipe = PipeFactory.newInMemoryInstance();
    }

    /**
     * Starts the training on the classifier.
     * @throws IllegalStateException When the classifier has been deserialized from an external source.
     */
    public void train() throws IllegalStateException {
        if (locked) {
            throw new IllegalStateException("Classifier is locked for training.");
        }

        C45Trainer trainer = new C45Trainer(this.doPruning);
        InstanceList instances = new InstanceList(pipe);

        FeatureExtractor extractor = new FeatureExtractor(features);

        this.data.getTrainingSet().stream()
                .map(entry -> {
                    String featureString = extractor.toFeatureString(entry.getToken());
                    return new Instance(featureString, entry.getLabel(), entry.getToken(), null);
                })
                .forEach(instances::addThruPipe);

        tree = trainer.train(instances);
        trained = true;
    }


    /**
     * Starts the validation on a trained classifier.
     * @return A validation result containing stats about the validation.
     * @throws IllegalStateException When the classifier is locked for validation.
     */
    public ValidationResult validate() throws IllegalStateException {

        if (locked) {
            throw new IllegalStateException("Classifier is locked for validation.");
        }

        FeatureExtractor extractor = new FeatureExtractor(features);
        InstanceList validationList = new InstanceList(pipe);
        data.getValidationSet().stream()
                .map(entry -> {
                    String featureString = extractor.toFeatureString(entry.getToken());
                    return new Instance(featureString, entry.getLabel(), entry.getToken(), null);
                })
                .forEach(validationList::addThruPipe);

        ValidationResult result = new ValidationResult(data, positiveMatchLabel);
        tree.classify(validationList).forEach(result::addEntry);
        return result;
    }


    /**
     * Classifies the given input with one of the labels from the training data.
     * @param input The input to be classified
     * @return A {@link ClassificationResult}.
     */
    public ClassificationResult classify(String input) {

        FeatureExtractor extractor = new FeatureExtractor(features);

        String featureString = extractor.toFeatureString(input);
        Instance instance = new Instance(featureString, "null", input, null);
        InstanceList instances = new InstanceList(pipe);
        instances.addThruPipe(instance);

        Classification cls = tree.classify(instances.get(0));
        String classification = cls.getLabeling().getBestLabel().getEntry().toString();
        return new ClassificationResult(input, classification, cls.getLabeling().getBestValue());
    }

    /**
     * {@inheritDoc}
     */
    private void writeObject(java.io.ObjectOutputStream out) throws IOException {
        this.locked = true;
        out.defaultWriteObject();
    }

    /**
     * Builder for a new classifier.
     */
    public static class Builder {

        private DataSet dataSet;
        private boolean doPruning = false;
        private List<Feature> features = new ArrayList<>();
        private String positiveMatchLabel;
        private String negativeMatchLabel;

        /**
         * Creates a new and untrained classifier for the specified dataset
         * @param data The dataset used for training and evaluation.
         * @return A (untrained) classifier
         */
        public Classifier.Builder forDataset(DataSet data) {
            this.dataSet = data;
            return this;
        }

        /**
         * Enables pruning of the resulting C4.5 tree after training.
         * @return A classifier with pruning enabled.
         */
        public Classifier.Builder withPruningEnabled() {
            this.doPruning = true;
            return this;
        }

        /**
         * Adds all of the specified features to be used during training.
         * Note that calling this method will override all features that might have been set before!
         * @param features The list of features to be used during training.
         * @return An (untrained) classifier.
         */
        public Classifier.Builder withFeatures(Feature... features) {
            this.features = Arrays.asList(features);
            return this;
        }

        /**
         * Specifies the label the training data is labeled with considering the test data instance is a positive match.
         * @param label The label of a positive classified test instance.
         * @return An (untrained) classifier
         */
        public Classifier.Builder withPositiveMatchesLabeledAs(String label) {
            this.positiveMatchLabel = label;
            return this;
        }

        /**
         * Specifies the label the training data is labeled with considering the test data instance is a negative match
         * @param label The label of a negative classified test instance.
         * @return An (untrained) classifier
         */
        public Classifier.Builder withNegativeMatchesLabeledAs(String label) {
            this.negativeMatchLabel = label;
            return this;
        }

        /**
         * Returns the untrained classifier.
         * @return The classifier
         */
        public Classifier build() {
            Classifier retval = new Classifier(this.dataSet);
            retval.doPruning = this.doPruning;
            retval.features = this.features;
            retval.positiveMatchLabel = this.positiveMatchLabel;
            retval.negativeMatchLabel = this.negativeMatchLabel;
            return retval;
        }
    }
}
