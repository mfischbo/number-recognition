package net.fischboeck.c45.trainer;

import lombok.Getter;
import lombok.ToString;

/**
 * The result of a classification from the {@link Classifier}.
 *
 */
@Getter
@ToString
public class ClassificationResult {

    /**
     * The input that has been classified.
     */
    private String input;

    /**
     * The label that has been predicted
     */
    private String predictedLabel;

    /**
     * The certainty of the classification.
     * This is a number between 0.0 and 1.0
     */
    private double certainty;


    ClassificationResult(String input, String predictedLabel, double certainty) {
        this.input = input;
        this.predictedLabel = predictedLabel;
        this.certainty = certainty;
    }
}
