package net.fischboeck.c45.trainer;

import net.fischboeck.c45.features.Feature;
import net.fischboeck.c45.io.PipeFactory;

import java.io.Serializable;
import java.util.*;

/**
 * Extracts the features from a given input for all registered {@link Feature}s.
 * The internal format for a feature string is [name]=[value] separated by whitespaces.
 * Example: foo=1.0 bar=1.0 foobar=3.0
 */
class FeatureExtractor implements Serializable {

    // The list of features for this extractor.
    private List<Feature> features;

    /**
     * Creates a new feature extractor with the features specified.
     * @param features The features that should be extracted from an input
     */
    FeatureExtractor(Collection<Feature> features) {
        this.features = new ArrayList<>(features.size());
        this.features.addAll(features);
    }

    /**
     * Extracts all features from the specified input.
     * @param data The input string.
     * @return The feature string as used by ({@link cc.mallet.pipe.TokenSequenceParseFeatureString}
     */
    String toFeatureString(String data) {

        Map<String, Double> features = extractFeatures(data);
        StringBuffer b = new StringBuffer();
        features.entrySet().forEach(entry -> {
            b.append(entry.getKey())
                    .append(PipeFactory.FEATURE_VALUE_SEPARATOR)
                    .append(entry.getValue())
                    .append(" ");
        });
        return b.toString().trim();
    }

    private Map<String, Double> extractFeatures(String line) {
        Map<String, Double> retval = new HashMap<>();
        this.features.forEach(feature -> retval.put(feature.getName(), feature.getValue(line)));
        return retval;
    }
}
