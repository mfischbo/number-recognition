package net.fischboeck.c45.trainer;

import cc.mallet.classify.Classification;
import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class ValidationResult {

    private String truePositiveLabel;

    @Getter
    private int truePositives;

    @Getter
    private int falsePositive;

    @Getter
    private int trueNegatives;

    @Getter
    private int falseNegatives;

    private int evaluationSetSize;

    private List<String> falseClassificationExamples = new ArrayList<>();

    ValidationResult(DataSet data, String truePositiveLabel) {
        this.evaluationSetSize = data.getValidationSet().size();
        this.truePositiveLabel = truePositiveLabel;
    }

    void addEntry(Classification classification) {

        String classifiedAs = classification.getLabeling().getBestLabel().toString();
        String expected = classification.getInstance().getLabeling().getBestLabel().toString();

        if (classifiedAs.equals(expected)) {
            if (classifiedAs.equals(truePositiveLabel)) {
                truePositives++;
            } else {
                trueNegatives++;
            }
        } else {
            falseClassificationExamples.add(String.format("%s {%s} was labeled as {%s}",
                        classification.getInstance().getName(),
                        classification.getInstance().getTarget(),
                        classifiedAs));

            if (classifiedAs.equals(truePositiveLabel) && !truePositiveLabel.equals(expected)) {
                falsePositive++;
            }

            if (!classifiedAs.equals(truePositiveLabel)) {
                if (truePositiveLabel.equals(expected)) {
                    falseNegatives++;
                }
            }
        }
    }

    public void print() {

        System.out.println("\n============= RESULTS ==============");
        System.out.println("Samples #:\t\t " + evaluationSetSize);
        System.out.println("TP      #:\t\t " + truePositives);
        System.out.println("FP      #:\t\t " + falsePositive);
        System.out.println("TN      #:\t\t " + trueNegatives);
        System.out.println("FN      #:\t\t " + falseNegatives);
        System.out.println("\n-------- Scores -------------\n");
        System.out.println("Precision:\t\t " + String.format(Locale.ENGLISH, "%.4f", getPrecission()));
        System.out.println("Recall:\t\t\t " +  String.format(Locale.ENGLISH, "%.4f", getRecall()));
        System.out.println("Accuracy:\t\t " + String.format(Locale.ENGLISH, "%.4f", getAccuracy()));
        System.out.println("F1-Score:\t\t " +  String.format(Locale.ENGLISH, "%.4f", getF1Score()));
        System.out.println("\n-------- Examples ---------- \n");

        if (falseClassificationExamples.isEmpty()) {
            System.out.println(" No false classifications ʘ‿ʘ ");
        }

        for (int i=0; i < Math.min(20, falseClassificationExamples.size()); i++) {
            System.out.println(falseClassificationExamples.get(i));
        }
    }

    public double getPrecission() {
        return (truePositives * 1.d) / (truePositives + falsePositive);
    }

    public double getRecall() {
        return  truePositives * 1.d / (truePositives + falseNegatives);
    }

    public double getAccuracy() {
        return (truePositives *1.d + trueNegatives * 1.d) / (evaluationSetSize * 1.d);
    }

    public double getF1Score() {
        double precission = getPrecission();
        double recall = getRecall();
        return 2 * (precission * getRecall()) / (precission + recall);
    }
}
