package net.fischboeck.c45.io;

import cc.mallet.pipe.*;

import java.util.Arrays;

/**
 * Utility to create a suitable {@link Pipe} for the trainer.
 * Note that a pipe contains internal state and is thus bound to a {@link cc.mallet.classify.C45} that is being used.
 */
public class PipeFactory {

    public static final String FEATURE_VALUE_SEPARATOR = "=";

    public static Pipe newInMemoryInstance() {

        return new SerialPipes(Arrays.asList(
            new LineGroupString2TokenSequence(),
            new TokenSequenceParseFeatureString(true, true, FEATURE_VALUE_SEPARATOR),
            new TokenSequence2FeatureSequence(),
            new FeatureValueString2FeatureVector(),
            new Target2Label()
        ));
    }
}
