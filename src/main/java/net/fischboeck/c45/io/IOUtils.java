package net.fischboeck.c45.io;

import net.fischboeck.c45.trainer.Classifier;

import java.io.*;

/**
 * Methods to save and load a {@link Classifier} from and to disk.
 * Please note that the classifiers internal dataset will NOT be stored and thus the methods
 * {@link Classifier#train()} and {@link Classifier#validate()} will not execute as if on a newly created
 * classifier.
 * The purpose is to be able to load a trained {@link Classifier} from disk and use if for classifications only.
 */
public class IOUtils {

    /**
     * Saves the classifier in the specified file
     * @param classifier The classifier to be saved
     * @param output The output file where to store the classifier.
     * @return True if the operation was successful, false otherwise.
     */
    public static boolean saveTree(Classifier classifier, File output) {

        try (
                ObjectOutputStream oout = new ObjectOutputStream(new FileOutputStream(output))) {

            oout.writeObject(classifier);
            oout.flush();
            oout.close();
            return true;
        } catch (IOException ex) {
            return false;
        }
    }


    /**
     * Loads a {@link Classifier} from the specified file.
     * @param input The file from where to load the classifier.
     * @return The {@link Classifier} or {@code null} if the classifier could not be loaded.
     */
    public static Classifier loadTree(File input) {

        try (ObjectInputStream oin = new ObjectInputStream(new FileInputStream(input))) {
            Classifier classifier = (Classifier) oin.readObject();
            oin.close();
            return classifier;
        } catch (Exception ex) {
            return null;
        }
    }

    /**
     * Loads a {@link Classifier} from the specified input stream.
     * @param inputStream The input stream to load the classifier from.
     * @return The {@link Classifier} or {@code null} if the classifier could not be loaded.
     */
    public static Classifier loadTree(InputStream inputStream) {

        try (ObjectInputStream oin = new ObjectInputStream(inputStream)) {
            Classifier classifier = (Classifier) oin.readObject();
            oin.close();
            return classifier;
        } catch (Exception ex) {
            return null;
        }
    }
}
