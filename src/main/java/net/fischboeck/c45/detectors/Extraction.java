package net.fischboeck.c45.detectors;

import lombok.Getter;

@Getter
public class Extraction {

    private String value;

    private int start;
    private int end;

    Extraction(String value, int start, int end) {
        this.value = value;
        this.start = start;
        this.end = end;
    }
}
