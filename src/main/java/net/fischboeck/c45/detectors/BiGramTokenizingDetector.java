package net.fischboeck.c45.detectors;

import lombok.Getter;
import net.fischboeck.c45.trainer.ClassificationResult;
import net.fischboeck.c45.trainer.Classifier;
import net.fischboeck.c45.util.Constants;
import org.apache.commons.lang3.StringUtils;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.LinkedList;
import java.util.List;

public class BiGramTokenizingDetector {

    private final Classifier classifier;
    private final String positiveLabel;

    public BiGramTokenizingDetector(Classifier classifier) {
        this.classifier = classifier;
        this.positiveLabel = classifier.getPositiveMatchLabel();
    }

    public List<Extraction> extract(String input) {

        List<Extraction> retval = new LinkedList<>();
        List<BiToken> tokens = tokenize(input);
        for (BiToken token : tokens) {
            ClassificationResult result = classifier.classify(token.value);
            if (result.getPredictedLabel().equals(positiveLabel)) {
                retval.add(new Extraction(token.value, token.startPos, token.endPos));
            }
        }


        return retval;
    }

    List<BiToken> tokenize(String input) {
        input = StringUtils.stripEnd(input, Constants.WhitespaceString);

        List<BiToken> tokens = new LinkedList<>();

        CharacterIterator it = new StringCharacterIterator(input);

        boolean seenToken1 = false;
        boolean seenWhitespaceBetween = false;
        boolean seenToken2 = false;
        int startPos = 0;
        int token1EndPos = -1;

        while (it.current() != CharacterIterator.DONE) {

            if (!isWhitespace(it.current())) {

                // this is the first token
                if (!seenToken1) {
                    seenToken1 = true;
                    it.next();
                    continue;
                }

                if (seenToken1 && !seenWhitespaceBetween) {
                    it.next();
                    continue;
                }

                if (seenToken1 && seenWhitespaceBetween) {
                    // set the index where token 1 ends
                    if (token1EndPos == -1) {
                        token1EndPos = it.getIndex();
                    }

                    // we must be over token 2 then
                    seenToken2 = true;
                }
                it.next();
            } else {

                if (seenToken1 && seenToken2) {
                    // this is the end of the bi-gram
                    BiToken bi = new BiToken(input.substring(startPos, it.getIndex()), startPos, it.getIndex());
                    tokens.add(bi);
                    startPos = token1EndPos;
                    token1EndPos = -1;
                    it.setIndex(startPos);

                    seenToken1 = false;
                    seenToken2 = false;
                    seenWhitespaceBetween = false;
                    continue;
                } else {

                    if (seenToken1) {
                        seenWhitespaceBetween = true;
                    }

                    it.next();
                }
            }
        }
        BiToken lastToken = new BiToken(input.substring(startPos, it.getIndex()), startPos, it.getIndex());
        tokens.add(lastToken);
        return tokens;
    }

    private boolean isWhitespace(char character) {
        for (int i=0; i < Constants.Whitespace.length; i++) {
            if (character == Constants.Whitespace[i]) {
                return true;
            }
        }
        return false;
    }

    @Getter
    static class BiToken {
        private String value;
        private int startPos;
        private int endPos;

        BiToken(String value, int startPos, int endPos) {
            this.value = value;
            this.startPos = startPos;
            this.endPos = endPos;
        }
    }
}
