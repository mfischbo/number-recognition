package net.fischboeck.c45;

import net.fischboeck.c45.trainer.ClassificationResult;
import net.fischboeck.c45.trainer.Classifier;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class Forrest {

    private Map<String, Classifier> trees;

    public Forrest() {
        this.trees = new HashMap<>();
    }

    public void addNamedClassifier(String name, Classifier tree) {
        this.trees.put(name, tree);
    }


    public void evaluate(String input, String wantedLabel) {

        Map<String, ClassificationResult> results = new HashMap<>();
        this.trees.forEach((name, tree) -> {
            ClassificationResult result = tree.classify(input);
            if (result.getPredictedLabel().equals(wantedLabel)) {
                results.put(name, result);
            }
        });

        if (results.size() == 0) {
            System.out.println("None of the trees classified input with wanted label " + wantedLabel);
            return;
        }

        Entry<String, ClassificationResult> winner = results.entrySet().iterator().next();
        Iterator<Entry<String, ClassificationResult>> x = results.entrySet().iterator();
        while (x.hasNext()) {
            Entry<String, ClassificationResult> entry = x.next();
            if (entry.getValue().getCertainty() > winner.getValue().getCertainty()) {
                winner = entry;
            }
        }

        System.out.println("Winning classification is " + winner.getKey() + ". Classifying result as ["+winner.getValue().getPredictedLabel()+"] with certainity: " + winner
        .getValue().getCertainty());
    }
}
