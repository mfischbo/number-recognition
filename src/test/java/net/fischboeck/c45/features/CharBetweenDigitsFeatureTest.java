package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CharBetweenDigitsFeatureTest {

    @Test
    public void returnsCorrectName() {
        CharBetweenDigitsFeature feature = new CharBetweenDigitsFeature('.');
        assertEquals("CharBetweenDigits{.}", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        CharBetweenDigitsFeature feature = new CharBetweenDigitsFeature('.');
        assertEquals(1, feature.getValue("33.44"), 0.01d);
        assertEquals(0, feature.getValue("33"), 0.01d);
    }
}
