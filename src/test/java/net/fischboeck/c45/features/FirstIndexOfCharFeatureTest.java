package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FirstIndexOfCharFeatureTest {

    @Test
    public void returnsCorrectName() {
        FirstIndexOfCharFeature feature = new FirstIndexOfCharFeature('v', false);
        assertEquals("firstidx{v}", feature.getName());
    }

    @Test
    public void returnsCorrectValue_ForwardSearch() {

        FirstIndexOfCharFeature feature = new FirstIndexOfCharFeature('v', false);
        assertEquals(0.0d, feature.getValue("verbose"), 0.01d);
        assertEquals(1.d, feature.getValue("overall"), 0.01d);
    }

    @Test
    public void returnsCorrectValue_ReverseSearch() {

        FirstIndexOfCharFeature feature = new FirstIndexOfCharFeature('v', true);
        assertEquals(0.d, feature.getValue("malv"), 0.01d);
        assertEquals(1.d, feature.getValue("lava"), 0.01d);
    }
}
