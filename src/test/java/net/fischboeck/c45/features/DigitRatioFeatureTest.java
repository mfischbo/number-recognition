package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DigitRatioFeatureTest {

    @Test
    public void returnsCorrectName() {
        DigitRatioFeature feature = new DigitRatioFeature();
        assertEquals("digitRatio", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        DigitRatioFeature feature = new DigitRatioFeature();
        assertEquals(0, feature.getValue("Hello"), 0.01d);
        assertEquals(33.0d, feature.getValue("He2"), 0.01d);
        assertEquals(50.d, feature.getValue("He22"), 0.01d);
        assertEquals(75.d, feature.getValue("222L"), 0.01d);
        assertEquals(100.d, feature.getValue("12345"), 0.01d);
    }
}
