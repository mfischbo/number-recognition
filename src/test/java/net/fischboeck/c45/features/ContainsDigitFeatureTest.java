package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainsDigitFeatureTest {

    @Test
    public void returnsCorrectName() {
        ContainsDigitFeature feature = new ContainsDigitFeature();
        assertEquals("containsDigit", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        ContainsDigitFeature feature = new ContainsDigitFeature();
        assertEquals(0, feature.getValue("Hello World"), 0.01d);
        assertEquals(1, feature.getValue("This is 2017"), 0.01d);
    }
}
