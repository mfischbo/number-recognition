package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class WhitespaceCountFeatureTest {

    @Test
    public void returnsCorrectName() {
        WhitespaceCountFeature feature = new WhitespaceCountFeature();
        assertEquals("whitespaces", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        WhitespaceCountFeature feature = new WhitespaceCountFeature();
        assertEquals(0, feature.getValue("Hello"), 0.01d);
        assertEquals(1, feature.getValue("Hello World"), 0.01d);
        assertEquals(1, feature.getValue("Hello\u00A0World"), 0.01d);
        assertEquals(1, feature.getValue("Hello\u2060World"), 0.01d);
    }
}
