package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainsSpecialCharsTest {

    @Test
    public void returnsCorrectName() {
        ContainsSpecialChars feature = new ContainsSpecialChars();
        assertEquals("contains-special", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        ContainsSpecialChars feature = new ContainsSpecialChars();
        assertEquals(0, feature.getValue("Hello"), 0.01d);
        assertEquals(1, feature.getValue("This.is.21!"), 0.01d);
    }
}
