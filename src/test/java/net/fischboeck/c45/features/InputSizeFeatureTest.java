package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InputSizeFeatureTest {

    @Test
    public void returnsCorrectName() {
        InputSizeFeature feature = new InputSizeFeature();
        assertEquals("inputSize", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        InputSizeFeature feature = new InputSizeFeature();
        assertEquals(0, feature.getValue(""), 0.01d);
        assertEquals(5.d, feature.getValue("Hello"), 0.01d);
        assertEquals(0, feature.getValue(null), 0.01d);
    }
}
