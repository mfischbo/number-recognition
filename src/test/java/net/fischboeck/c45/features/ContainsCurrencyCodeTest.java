package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainsCurrencyCodeTest {

    @Test
    public void returnsCorrectName() {

        ContainsCurrencyCode feature = new ContainsCurrencyCode();
        assertEquals("cc-code", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        ContainsCurrencyCode feature = new ContainsCurrencyCode();
        assertEquals(0, feature.getValue("Hello world"), 0.01d);
        assertEquals(1, feature.getValue("EUR"), 0.01d);
        assertEquals(1, feature.getValue("€ 20"), 0.01d);
    }
}
