package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DigitCountFeatureTest {

    @Test
    public void returnsCorrectName() {
        DigitCountFeature feature = new DigitCountFeature();
        assertEquals("digitcount", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        DigitCountFeature feature = new DigitCountFeature();
        assertEquals(0, feature.getValue("Hello"), 0.01d);
        assertEquals(3, feature.getValue("Hello 123"), 0.01d);
    }
}
