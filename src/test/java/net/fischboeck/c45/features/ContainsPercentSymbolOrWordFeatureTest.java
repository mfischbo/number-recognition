package net.fischboeck.c45.features;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ContainsPercentSymbolOrWordFeatureTest {

    @Test
    public void returnsCorrectName() {
        ContainsPercentSymbolOrWordFeature feature = new ContainsPercentSymbolOrWordFeature();
        assertEquals("percentSymWord", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        ContainsPercentSymbolOrWordFeature feature = new ContainsPercentSymbolOrWordFeature();
        assertEquals(0, feature.getValue("This is some test sentence"), 0.001d);
        assertEquals(1.d, feature.getValue("This % symbol is one"), 0.001d);
        assertEquals(1.d, feature.getValue("Wir haben 3 Prozent"), 0.001d);
        assertEquals(1.d, feature.getValue("This contains 3 percent of..."), 0.001d);
    }
}
