package net.fischboeck.c45.features;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

public class CharacterCountFeatureTest {


    @Test
    public void returnsCorrectName() throws Exception {
        CharacterCountFeature feature = new CharacterCountFeature('.');
        assertEquals("count[.]", feature.getName());
    }

    @Test
    public void returnsCorrectCount() throws Exception {
        CharacterCountFeature feature = new CharacterCountFeature('.');
        assertEquals(3, feature.getValue("Hello.World.Test."), 0.01d);
    }
}
