package net.fischboeck.c45.features;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class MaxDigitGroupLengthTest {

    @Test
    public void returnsCorrectName() {
        MaxDigitGroupLength feature = new MaxDigitGroupLength();
        Assert.assertEquals("MaxDigitGroupLength", feature.getName());
    }

    @Test
    public void returnsCorrectValue() {
        MaxDigitGroupLength feature = new MaxDigitGroupLength();
        assertEquals(2, feature.getValue("22 1 14"), 0.01d);
        assertEquals(0, feature.getValue("Hello"), 0.01d);
    }
}
