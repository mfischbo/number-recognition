package net.fischboeck.c45.trainer;

import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class DataSetTest {

    @Test
    public void init_shufflesDataSet() {

        ArrayList<DataSet.Entry> entries = new ArrayList<>();
        for (int i=0; i < 20; i++) {
            entries.add(new DataSet.Entry("" + i, "yes"));
        }

        boolean continuous = true;
        DataSet ds = new DataSet(entries, 0.d);
        for (int i=0; i < ds.getTrainingSet().size(); i++) {
            DataSet.Entry e = ds.getTrainingSet().get(i);
            if (Integer.parseInt(e.getToken()) != i) {
                continuous = false;
            }
        }
        assertFalse(continuous);
    }

    @Test
    public void init_splitsCorrectly() {

        ArrayList<DataSet.Entry> entries = new ArrayList<>();
        for (int i=0; i < 100; i++) {
            entries.add(new DataSet.Entry("" + i, "yes"));
        }

        DataSet ds = new DataSet(entries, 25.d);
        assertEquals(75, ds.getTrainingSet().size());
        assertEquals(25, ds.getValidationSet().size());
        assertEquals(100, ds.getTrainingSet().size() +  ds.getValidationSet().size());
    }
}
