package net.fischboeck.c45.trainer;

import net.fischboeck.c45.features.Feature;
import net.fischboeck.c45.features.InputSizeFeature;
import org.junit.Test;

import java.util.Collections;

import static junit.framework.Assert.assertEquals;

public class FeatureExtractorTest {

    @Test
    public void toFeatureString_createsCorrectString() {

        Feature f = new InputSizeFeature();
        FeatureExtractor extractor = new FeatureExtractor(Collections.singletonList(f));
        String testString = extractor.toFeatureString("Hello");

        assertEquals(f.getName() + "=" + f.getValue("Hello"), testString);
    }
}
