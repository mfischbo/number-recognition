package net.fischboeck.c45.detectors;

import net.fischboeck.c45.detectors.BiGramTokenizingDetector.BiToken;
import net.fischboeck.c45.trainer.ClassificationResult;
import net.fischboeck.c45.trainer.Classifier;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BiGramTokenizingDetectorTest {

    private BiGramTokenizingDetector detector;

    private Classifier classifier;


    @Test
    public void extractsBiGramsCorrectly_easyInput() {

        classifier = mock(Classifier.class);
        detector = new BiGramTokenizingDetector(classifier);

        List<BiToken> tokens = detector.tokenize("This is just a test to see if the tokenizer works.");
        assertEquals(10, tokens.size());
        assertEquals("This is", tokens.get(0).getValue());
        assertEquals("a test", tokens.get(3).getValue());
        assertEquals("tokenizer works.", tokens.get(9).getValue());

        // assert start & end positions
        // This is
        assertEquals(0, tokens.get(0).getStartPos());
        assertEquals(7, tokens.get(0).getEndPos());

        // a test
        assertEquals(13, tokens.get(3).getStartPos());
        assertEquals(19, tokens.get(3).getEndPos());

        // tokenizer works
        assertEquals(34, tokens.get(9).getStartPos());
        assertEquals(50, tokens.get(9).getEndPos());
    }

    @Test
    public void extractsBiGramsCorrectly_inputWithMultipleSpaces() {

        detector = new BiGramTokenizingDetector(mock(Classifier.class));
        List<BiToken> bigrams = detector.tokenize("A  test with multiple  whitespaces     in it.  ");
        assertEquals(6, bigrams.size());

        assertEquals("A  test", bigrams.get(0).getValue());
        assertEquals(0, bigrams.get(0).getStartPos());
        assertEquals(7, bigrams.get(0).getEndPos());

        assertEquals("test with", bigrams.get(1).getValue());
        assertEquals(3, bigrams.get(1).getStartPos());
        assertEquals(12, bigrams.get(1).getEndPos());

        assertEquals("with multiple", bigrams.get(2).getValue());
        assertEquals(8, bigrams.get(2).getStartPos());
        assertEquals(21, bigrams.get(2).getEndPos());

        assertEquals("in it.", bigrams.get(5).getValue());
        assertEquals(39, bigrams.get(5).getStartPos());
        assertEquals(45, bigrams.get(5).getEndPos());
    }


    @Test
    public void returnsCorrectExtractions() {

        ClassificationResult positive = mock(ClassificationResult.class);
        when(positive.getPredictedLabel()).thenReturn("yes");

        ClassificationResult negative = mock(ClassificationResult.class);
        when(negative.getPredictedLabel()).thenReturn("no");

        classifier = mock(Classifier.class);
        when(classifier.getPositiveMatchLabel()).thenReturn("yes");
        when(classifier.classify("Foobar is")).thenReturn(positive);
        when(classifier.classify("is a")).thenReturn(negative);
        when(classifier.classify("a word!")).thenReturn(negative);

        detector = new BiGramTokenizingDetector(classifier);

        List<Extraction> extractions = detector.extract("Foobar is a word!");
        assertEquals(1, extractions.size());

        Extraction ex = extractions.get(0);
        assertEquals("Foobar is", ex.getValue());
        assertEquals(0, ex.getStart());
        assertEquals(9, ex.getEnd());
    }
}
