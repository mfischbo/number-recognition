package net.fischboeck.generator.sources;

import net.fischboeck.generator.api.PipelineBuilder;
import net.fischboeck.generator.api.Sink;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class NumberSourceTest {

    @Test
    public void emit_createsNewNumber() {

        NumberSource source = new NumberSource();
        Sink successor = mock(Sink.class);

        PipelineBuilder.startWith(source)
            .andAppend(successor);

        source.emit();
        verify(successor, times(1)).push(any());
    }
}
